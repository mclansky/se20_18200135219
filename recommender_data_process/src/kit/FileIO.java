package kit;

import function.GobleVar;
import function.GobleTool;
import domain.Movie;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

public class FileIO
{
	private static ObjectMapper objectMapper = new ObjectMapper();

	//DONE:读取映射已序列化后的信息文件并将其结果存入全局变量
	static public void readMappingItem(String file_path,int isItem) throws IOException
	{
		ArrayList<String> contentList = readFile(file_path);
		if (isItem==1)
			GobleVar.item_to_user.putAll(objectMapper.readValue(contentList.get(0), new TypeReference<HashMap<Integer, HashMap<Integer, Float>>>(){}));
		else
			GobleVar.user_to_item.putAll(objectMapper.readValue(contentList.get(0), new TypeReference<HashMap<Integer, HashMap<Integer, Float>>>(){}));

		//DONE:提升效率，直接读取序列化后的文件为集合，无需一条一条直接读取 V3.0
/*		for (int i=0;i<contentList.size();i++)
		{
			String conten[] = contentList.get(i).split("::");
			if (isItem==1)
				GobleVar.item_to_user.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1],new TypeReference<HashMap<Integer,Float>>(){}));
			else
				GobleVar.user_to_item.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1],new TypeReference<HashMap<Integer,Float>>(){}));
		}*/
		GobleTool.printInfo("文件："+file_path+",读取成功!");
	}

	//DONE:读取视频信息文件并将其结果存入全局变量
	static public void readMovieList(String file_path) throws IOException
	{
		ArrayList<String> contentList = readFile(file_path);

		for (int i=0;i<contentList.size();i++)
		{
			//TODO:效率可以继续优化，对整个List集合进行序列化，读取时读取整个集合无需反复调用ObjectMapper
			//MARK:因数据量较少，效率提升有限，大概50-100ms，此方法也不会频繁调用暂不优化
			String conten[] = contentList.get(i).split("::");
			GobleVar.movieList.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1], new TypeReference<Movie>(){}));
		}
	}

	//DONE:读取文件到一个数组
	static public ArrayList<String> readFile(String file_path) throws IOException
	{
		File file;
		FileReader fr;
		BufferedReader br;
		GobleTool.printInfo("正在读入文件："+file_path+",请稍后...");
		file = new File(file_path);
		if (!file.exists())
		{
			return null;
		}

		fr = new FileReader(file);
		br = new BufferedReader(fr);

		String lineText;
		ArrayList<String> result = new ArrayList<>();
		while(br.ready())
		{
			lineText = br.readLine();
			result.add(lineText);
		}
		br.close();
		return result;
	}

	//DONE:向文件写入内容，参数为一个String数组
	static public void writeFile(String file_path,ArrayList<String> content) throws IOException
	{
		File file;
		FileWriter fw;
		PrintWriter pw;

		file = new File(file_path);
		File fileParent = file.getParentFile();
		if(!fileParent.exists())
		{
			fileParent.mkdirs();
		}
		if (!file.exists())
		{
			file.createNewFile();
		}
		fw = new FileWriter(file);
		pw = new PrintWriter(fw);

		for(int i=0;i<content.size();i++)
		{
			pw.println(content.get(i));
		}

		pw.close();
	}

	//DONE:向文件末尾追加写入内容，参数为一个String
	static public void writeFile(String file_path,String content)
	{
		FileWriter fw = null;

		try
		{
			//MARK:如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f=new File(file_path);
			File fileParent = f.getParentFile();
			if(!fileParent.exists())
			{
				fileParent.mkdirs();
			}
			fw = new FileWriter(f, true);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(content);
		pw.flush();
		try
		{
			fw.flush();
			pw.close();
			fw.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	//DONE:删除文件
	static public boolean deleteFile(String sPath)
	{
		boolean flag = false;
		File file = new File(sPath);
		//MARK:判断路径为文件且不为空则进行删除
		if (file.isFile() && file.exists())
		{
			file.delete();
			flag = true;
		}
		return flag;
	}
}