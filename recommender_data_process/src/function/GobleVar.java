package function;

import domain.Movie;
import domain.Rating;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class GobleVar
{
    public static boolean isDebug = true;

    public static ArrayList<String> moviesFile;
    public static ArrayList<String> ratingsFile;
    public static HashMap<Integer,Movie> movieList = new HashMap<>();
    public static ArrayList<Rating>  ratingList= new ArrayList<>();

    //TODO:对Integer进行优化，用无符号Short代替，缩短Hash查找时间
    public static ConcurrentHashMap<Integer, HashMap<Integer,Float>> item_to_user = new ConcurrentHashMap<>();    //MovieId:key
    public static ConcurrentHashMap<Integer, HashMap<Integer,Float>> user_to_item = new ConcurrentHashMap<>();    //UserId:key
}
