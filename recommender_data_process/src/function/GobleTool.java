package function;

import java.util.*;

public class GobleTool
{
    private static Scanner scanner = new Scanner(System.in);
    public static void printInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("==================================");
    }
    public static void printWaitInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("Info:请输入任意字符按回车继续");
        System.out.println("==================================");
        scanner.next();
    }
    public static void cleanner()
    {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    public static String printHelp()
    {
        String text = "";
        text += "================================================\n";
        text += "操作菜单\n";
        text += "执行操作'0'之前，需要执行操作'1.1'为其创建缓存\n";
        text += "请严格按照操作菜单的指令进行操作\n";
        text += "================================================\n";
        text += "0 - 通过缓存文件加载两个映射关系(缓存文件加载)\n";
        text += "1 - 通过实时计算加载两个映射关系(实时计算加载)\n";
        text += "1.1 - 执行操作'1'并创建其缓存文件\n";
        text += "================================================\n";
        text += "2 - 继续创建视频推送信息结果缓存(继续上次进度)\n";
        text += "3 - 重新创建视频推送信息结果缓存(清空上次进度)\n";
        text += "================================================\n";
        text += "4 - 开启调试模式\n";
        text += "5 - 关闭调试模式\n";
        text += "================================================\n";
        text += "exit - 退出\n";
        text += "================================================\n";
        text += "请输入指令代码：\n";
        System.out.println(text);
        return text;
    }

    public static <k, v> List<HashMap<k, v>> mapChunk(HashMap<k, v> chunkMap, int chunkNum)
    {
        if (chunkMap == null || chunkNum <= 0) {
            List<HashMap<k, v>> list = new ArrayList<>();
            list.add(chunkMap);
            return list;
        }
        Set<k> keySet = chunkMap.keySet();
        Iterator<k> iterator = keySet.iterator();
        int i = 1;
        List<HashMap<k, v>> total = new ArrayList<>();
        HashMap<k, v> tem = new HashMap<>();
        while (iterator.hasNext()) {
            k next = iterator.next();
            tem.put(next, chunkMap.get(next));
            if (i == chunkNum) {
                total.add(tem);
                tem = new HashMap<>();
                i = 0;
            }
            i++;
        }
        if (!tem.isEmpty() && tem.size()!=0) {
            total.add(tem);
        }
        return total;
    }
}