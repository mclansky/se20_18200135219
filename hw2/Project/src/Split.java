public class Split
{
    //https://gitee.com/mclansky/se20_18200135219.git
    //18200135219_宋清卿_作业2
    public static void main(String[] args)
    {
        String str = "abc苏州abc科技abc大学abc是一个abc在苏州的大学abc";
        String result[] = split(str,"abc");
        System.out.println("原始字符串："+str);
        for (int i=0;i<result.length;i++)
            System.out.println(result[i]);
    }
    public static String[] split(String str,String key)
    {
        //本切割方法考虑到了分隔符在开头、分隔符在结尾、分隔符为空值以及出现连续分隔符的情况
        //未使用split()、equals()、subString()等方法
        String[]  result,tempResult = new String[str.length()];
        int count=0,point = 0;//对字符串进行分割操作的循环
        for (int i=0;i<str.length() - key.length() + 1;i++)
        {
            String nowStr = "";//获取当前截取的字符串
            for(int s=i;s<i+key.length();s++)
                nowStr += str.charAt(s);
            boolean isEquals = true;//标记当前截取的字符串是否与key相等
            for (int s=0;s<key.length();s++)
                isEquals = (nowStr.charAt(s)==key.charAt(s))&&isEquals;
            if ((isEquals||key.length()==0)&&i<str.length())
            {
                tempResult[count] = "";
                for(int s=point;s<(key.length()==0?i+1:i);s++)//截取当前分隔符前的字符串
                    tempResult[count] += str.charAt(s);
                point = i+(key.length()==0?1:key.length());//待截取字符串头标记指针
                count = (i!=0||key.length()==0)?count+1:count;//统计已截取字符串的个数
            }
        }//检测结尾是否还有待分割字符串
        String temp = "";
        for(int s=point;s<str.length();s++)
            temp += str.charAt(s);
        if (temp.length()!=0)
            tempResult[count++] = temp;
        result = new String[count];//对字符串数组进行整理，去除空值项
        for (int i=0;i<tempResult.length&&tempResult[i]!=null;i++)
                result[i]=tempResult[i];
        return result;
    }
}
