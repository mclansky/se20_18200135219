public class Split_test
{
    //https://gitee.com/mclansky/se20_18200135219.git
    //18200135219_宋清卿_作业2
    public static void main(String[] args)
    {
        testSplitNormal();
        testSplitBlank();
    }

    public static void testSplitNormal()
    {
        String oriResStr[] = {"苏州","科技","大学","是一个","在苏州的大学"};
        String str[] = Split.split("abc苏州abc科技abc大学abc是一个abc在苏州的大学abc","abc");
        boolean isEqual = true;
        if (oriResStr.length!=str.length)
            isEqual = false;
        else
            for (int i=0;i<str.length;i++)
                if (!str[i].equals(oriResStr[i]))
                    isEqual = false;
        if (isEqual)
            System.out.println("testSplitNormal() OK!");
        else
            System.out.println("testSplitNormal() NO!");
    }

    public static void testSplitBlank()
    {
        String oriResStr[] = {};
        String str[] = Split.split("","");
        boolean isEqual = true;
        //先判断两个数组长度
        if (oriResStr.length!=str.length||str.length!=0)
            isEqual = false;
        if (isEqual)
            System.out.println("testSplitBlank() OK!");
        else
            System.out.println("testSplitBlank() NO!");
    }
}
