package Kit;

import Func.Goble;
import Func.GobleTool;

import java.io.*;

import java.util.*;

import Object.Rating;
import Object.Movie;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

public class FileIO
{
	public static void readEndingResult(String file_path) throws IOException
	{
		File file;
		FileReader fr;
		BufferedReader br;
		GobleTool.printInfo("正在读入文件："+file_path+",请稍后...");
		file = new File(file_path);
		if (file.exists())
		{
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String lineText;
			ObjectMapper objectMapper = new ObjectMapper();
			while(br.ready())
			{
				lineText = br.readLine();
				String conten[] = lineText.split("::");
				Goble.endingResult.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1], new TypeReference<List<Map.Entry<Integer,Double>>>(){}));
			}
			fr.close();
			br.close();
		}
	}

	public static void readMappingItem(String file_path,int isItem) throws IOException
	{
		File file;
		FileReader fr;
		BufferedReader br;
		GobleTool.printInfo("正在读入文件："+file_path+",请稍后...");
		file = new File(file_path);
		if (file.exists())
		{
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String lineText;
			ObjectMapper objectMapper = new ObjectMapper();
			while(br.ready())
			{
				lineText = br.readLine();
				String conten[] = lineText.split("::");
				if (isItem==1)
				{
					Goble.item_to_user.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1],new TypeReference<HashMap<Integer,Float>>(){}));
				}
				else
				{
					Goble.user_to_item.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1],new TypeReference<HashMap<Integer,Float>>(){}));
				}
			}
			GobleTool.printInfo("文件："+file_path+",读取成功!");
			fr.close();
			br.close();
		}
	}

	public static void readMovieList(String file_path) throws IOException
	{
		File file;
		FileReader fr;
		BufferedReader br;
		GobleTool.printInfo("正在读入文件："+file_path+",请稍后...");
		file = new File(file_path);
		if (file.exists())
		{
			fr = new FileReader(file);
			br = new BufferedReader(fr);

			String lineText;
			ObjectMapper objectMapper = new ObjectMapper();
			while(br.ready())
			{
				lineText = br.readLine();
				String conten[] = lineText.split("::");
				Goble.movieList.put(Integer.parseInt(conten[0]),objectMapper.readValue(conten[1], new TypeReference<Movie>(){}));
			}
			fr.close();
			br.close();
		}
	}

	public ArrayList readFile(String file_path) throws IOException
	{
		File file;
		FileReader fr;
		BufferedReader br;
		GobleTool.printInfo("正在读入文件："+file_path+",请稍后...");
		file = new File(file_path);
		if (!file.exists())
		{
			return null;
		}

		fr = new FileReader(file);
		br = new BufferedReader(fr);

		String lineText;
		ArrayList<String> result = new ArrayList<>();
		while(br.ready())
		{
			lineText = br.readLine();
			result.add(lineText);
		}

		br.close();
		return result;
	}

	public void writeFile(String file_path,ArrayList<String> content) throws IOException
	{
		File file;
		FileWriter fw;
		PrintWriter pw;

		file = new File(file_path);
		File fileParent = file.getParentFile();
		if(!fileParent.exists())
		{
			fileParent.mkdirs();
		}
		if (!file.exists())
		{
			file.createNewFile();
		}
		fw = new FileWriter(file);
		pw = new PrintWriter(fw);

		for(int i=0;i<content.size();i++)
		{
			pw.println(content.get(i));
		}

		pw.close();
	}

	public void writeFile(String file_path,String content)
	{
		FileWriter fw = null;

		try
		{
			//如果文件存在，则追加内容；如果文件不存在，则创建文件
			File f=new File(file_path);
			File fileParent = f.getParentFile();
			if(!fileParent.exists())
			{
				fileParent.mkdirs();
			}
			fw = new FileWriter(f, true);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		PrintWriter pw = new PrintWriter(fw);
		pw.println(content);
		pw.flush();
		try
		{
			fw.flush();
			pw.close();
			fw.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public boolean deleteFile(String sPath)
	{
		boolean flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists())
		{
			file.delete();
			flag = true;
		}
		return flag;
	}
}
