package Program;

import java.util.*;

public class Test2
{

    public static void main(String[] args)
    {
        test2();
    }
    public static void test2() {
        Integer count =1000000;
        Random random =new Random();
        Map<String, String> map = new HashMap<String, String>();
        for (int i = 0; i < count; i++) {
            map.put(i+"", i+"");
        }
        long time1 = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            map.get(i+"");
        }
        long time2 = System.currentTimeMillis();
        System.out.println("HashMap time:" + (time2 - time1));
        ////////////////////////////////////////////////////////////////////////
        Map<String, String> linkedMap = new LinkedHashMap<String, String>();
        for (int i = 0; i < count; i++) {
            linkedMap.put(i+"", i+"");
        }

        time1 = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            linkedMap.get(i+"");
        }
        time2 = System.currentTimeMillis();
        System.out.println("LinkedHashMap time:" + (time2 - time1));
        ////////////////////////////////////////////////////////////////////////
        Map<String, String> treeMap = new TreeMap<String, String>();
        for (int i = 0; i < count; i++) {
            treeMap.put(i+"", i+"");
        }

        time1 = System.currentTimeMillis();
        for (int i = 0; i < count; i++) {
            treeMap.get(i+"");
        }
        time2 = System.currentTimeMillis();
        System.out.println("TreeMap time:" +  (time2 - time1));
    }
}
