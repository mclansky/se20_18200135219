package Program;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public abstract class Test55
{
    public static void main(String[] args)
    {
        HashSet<Double> hashSetBig = new HashSet<>();
        HashSet<Double> hashSetSmall = new HashSet<>();

        for (int i=0;i<1000000;i++)
        {
            hashSetBig.add(Math.random());
        }
        for (int i=0;i<100000;i++)
        {
            hashSetSmall.add(Math.random());
        }
        /*long time1 = System.currentTimeMillis();
        hashSetBig.retainAll(hashSetSmall);
        long time2 = System.currentTimeMillis();
        System.out.println("HashMap time:" + (time2 - time1));*/

        long time11 = System.currentTimeMillis();
        hashSetSmall.retainAll(hashSetBig);
        long time22 = System.currentTimeMillis();
        System.out.println("HashMap time:" + (time22 - time11));


        /*long time111 = System.currentTimeMillis();
        for (Double i:hashSetSmall)
        {
            hashSetBig.remove(i);
        }
        long time222 = System.currentTimeMillis();*/
        //System.out.println("HashMap time:" + (time222 - time111));
    }
}
