package Func;

import java.util.*;

public class GobleTool
{
    private static Scanner scanner = new Scanner(System.in);
    public static void printInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("==================================");
    }
    public static void printWaitInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("Info:请输入任意字符按回车继续");
        System.out.println("==================================");
        scanner.next();
    }
    public static void cleanner()
    {
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    public static String printHelp()
    {
        String text = "==================================\n";
        text += "==================================\n";
        text += "操作菜单\n";
        text += "实时计算功能序号后加'.1'可创建缓存\n";
        text += "==================================\n";
        text += "0.一键初始化操作(缓存处理)\n";
        text += "1.一键初始化操作(实时计算)\n";
        text += "2.读取并初始化原始数据文件(实时计算)\n";
        text += "3.加载视频映射关系(实时计算)\n";
        text += "==================================\n";
        text += "4.根据视频ID进行相关视频推送(实时计算)\n";
        text += "5.根据视频ID进行相关视频推送(缓存版本)\n";
        text += "5.1.加载最终结果集至内存(内存版本)\n";
        text += "5.2.根据视频ID进行相关视频推送(内存版本)\n";
        text += "==================================\n";
        text += "6.开启调试模式\n";
        text += "7.关闭调试模式\n";
        text += "8.创建无缓存视频的推送信息缓存(缓存处理)\n";
        text += "9.重新创建视频推送信息缓存(缓存处理)\n";
        text += "exit.退出\n";
        text += "==================================\n";
        text += "请输入指令代码：\n";
        System.out.println(text);
        return text;
    }

    public static <k, v> List<LinkedHashMap<k, v>> mapChunk(LinkedHashMap<k, v> chunkMap, int chunkNum)
    {
        if (chunkMap == null || chunkNum <= 0) {
            List<LinkedHashMap<k, v>> list = new ArrayList<>();
            list.add(chunkMap);
            return list;
        }
        Set<k> keySet = chunkMap.keySet();
        Iterator<k> iterator = keySet.iterator();
        int i = 1;
        List<LinkedHashMap<k, v>> total = new ArrayList<>();
        LinkedHashMap<k, v> tem = new LinkedHashMap<>();
        while (iterator.hasNext()) {
            k next = iterator.next();
            tem.put(next, chunkMap.get(next));
            if (i == chunkNum) {
                total.add(tem);
                tem = new LinkedHashMap<>();
                i = 0;
            }
            i++;
        }
        if (!tem.isEmpty() && tem.size()!=0) {
            total.add(tem);
        }
        return total;
    }
}
