package Func;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import Object.Rating;
import Object.Movie;

public class Goble
{
    public static boolean isDebug = true;

    public static ArrayList<String> moviesFile;
    public static ArrayList<String> ratingsFile;
    public static HashMap<Integer,Movie> movieList = new HashMap<Integer,Movie>();
    public static ArrayList<Rating>  ratingList= new ArrayList();
    public static ConcurrentHashMap<Integer, HashMap<Integer,Float>> item_to_user = new ConcurrentHashMap<>();    //MovieId:key
    public static ConcurrentHashMap<Integer, HashMap<Integer,Float>> user_to_item = new ConcurrentHashMap<>();    //UserId:key

    public static ConcurrentHashMap<Integer, List<Map.Entry<Integer,Double>>> endingResult = new ConcurrentHashMap<>();    //UserId:key
}
