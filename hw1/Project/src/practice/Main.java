package practice;
public class Main
{
    public static void main(String[] args)
    {
        Sort sort = new Sort();
        int array1[] = {24,21,1,86,43,65,33,91,93,55};
        int array2[] = {24,21,1,86,43,65,33,91,93,55};
        int array3[] = {24,21,1,86,43,65,33,91,93,55};

        System.out.println("排序前的数组：");
        for (int i:array1)
            System.out.print(i + "\t");
        System.out.println();

        sort.selectSort(array1);
        sort.bubbleSort(array2);
        sort.insetSort(array3);
    }
}
