package practice;
public class Sort
{
    public int[] selectSort(int array[])
    {
        System.out.println("(选择排序)");
        System.out.println("排序过程：");
        for (int i=0;i<array.length;i++)
        {
            int min = i;
            for (int j=i;j<array.length;j++)
            {
                if (array[j]<array[min])
                    min = j;
            }
            if (min!=i)
            {
                int temp = array[i];
                array[i]=array[min];
                array[min]=temp;
            }
            for (int k:array)
                System.out.print(k + "\t");
            System.out.println();
        }
        return array;
    }

    public int[] bubbleSort(int array[])
    {
        System.out.println("(冒泡排序)");
        System.out.println("排序过程：");
        for(int i=0;i<array.length;i++)
        {
            for(int j=0;j<array.length-1-i;j++)
            {
                if (array[j]>array[j+1])
                {
                    int temp = array[j];
                    array[j]=array[j+1];
                    array[j+1]=temp;
                }
            }
            for (int k:array)
                System.out.print(k + "\t");
            System.out.println();
        }
        return array;
    }

    public int[] insetSort(int array[])
    {
        System.out.println("(插入排序)");
        System.out.println("排序过程：");
        for(int i=1;i<array.length;i++)
        {
            int num=array[i];
            int j;
            for (j=i;(j-1)>=0&&array[j-1]>num;j--)
            {
                array[j]=array[j-1];
            }
            array[j]=num;
            for (int k:array)
                System.out.print(k + "\t");
            System.out.println();
        }
        return array;
    }
}
