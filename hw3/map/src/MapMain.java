import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;


public class MapMain {

	public static void main(String[] args) {
		Map<String,Integer> map = new HashMap<>();
		map.put("Lucy", new Integer(5));
		map.put("Lily", new Integer(4));
		map.put("Han Meimei", new Integer(5));
		map.put("Jim", new Integer(3));
		map.put("Zou Encen", new Integer(3));
		map.put("Song Qingqing", new Integer(2));

		String key = "Zou Encen";
		Integer value = null;
		if (map.containsKey(key))
		{
			value = map.get(key);
			System.out.println("Use key:" + key + " find value:" + value);
		}
		else
		{
			System.out.println("Does not contain key:" + key);
		}

		key = "Song Qingqing";
		if (map.containsKey(key))
		{
			value = map.get(key);
			System.out.println("Use key:" + key + " find value:" + value);
		}
		else
		{
			System.out.println("Does not contain key:" + key);
		}

		key = "Tom";
		if (map.containsKey(key))
		{
			value = map.get(key);
			System.out.println("Use key:" + key + " find value:" + value);
		}
		else
		{
			System.out.println("Does not contain key:" + key);
		}

		// 迭代器与下标的概念是类似的，有些数据结构没有下标操作（例如，链表，哈希映射），则我们访问这些数据结构的元素时，需要使用迭代器iterator来代替下标index。
		// Map.Entry叫做map的条目，一个条目是一个key-value对
		// Iterator.next()操作，是使迭代器移动到map的下一个下元素上
		// Entry.getKey()方法，用来从当前key-value对中获取key
		// Entry.getValue()用来从当前key-value对中获取value
		Iterator it = map.entrySet().iterator();
		Map.Entry entry;

		for (int i=0;it.hasNext();i++)
		{
			entry = (Map.Entry) it.next();
			key=(String) entry.getKey();
			value = (Integer) entry.getValue();
			System.out.println("Use iterator to get the "+i+" entry is Key :"+key+"  Value :"+value);
		}
	}

}
