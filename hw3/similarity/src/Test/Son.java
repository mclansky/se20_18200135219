package Test;

public class Son extends Father
{
    private float money;

    public float getMoney()
    {
        return this.money;
    }

    public void setMoney(float money)
    {
        this.money=money;
    }
}
