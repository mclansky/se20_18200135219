package Test;

public class Test
{
    public static void main(String[] args666)
    {
        Father father = new Father();
        SonsSon sonsSon = new SonsSon();
        father.setA("张三");
        father.setB(110);
        father.setDepartment("人事部");

        sonsSon.setA("李四");
        sonsSon.setB(112);
        sonsSon.setDepartment("教育部");
        sonsSon.setMoney(10000);
        sonsSon.setSex("男");

        System.out.println(sonsSon.getA());
        System.out.println(sonsSon.getDepartment());

        System.out.println(father.getA());
    }
    //属性是一种特殊的变量。格式相同(没有权限)。
}
