package Test;

public class Father
{
    private String a;
    private int b;
    private String department;

    public String getA()
    {
        return this.a;
    }

    public int getB()
    {
        return this.b;
    }

    public String getDepartment()
    {
        return this.department;
    }

    public void setA(String a)
    {
        this.a = a;
    }

    public void setB(int b)
    {
        this.b = b;
    }

    public void setDepartment(String department)
    {
        this.department=department;
    }

}

//只有小括号里面的叫参数

//一个java文件，里面有且仅有1个公有的类。
//一个类由属性和方法组成。
//属性简单，方法复杂。方法的标志是小括号。
//一个属性由3部分组成，分别是：权限，属性的类型，属性名
//一个方法由4部分组成，分别实：权限，方法的返回类型，方法体（方法头后的，花括号中的）