package function;

import kit.FileIO;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class Core
{
    public static void readMovieInfo() throws IOException
    {
        //DONE:读入视频信息文件
        FileIO io = new FileIO();
        io.readMovieList("d:/teamdata/cache/system/movieList.dat");
        //DONE:读入最终结果文件
        String path="d:/teamdata/cache/endingResult.dat";
        FileIO fileIO = new FileIO();
        fileIO.readEndingResult(path);
    }

    public static void recommendByMoiveId(int moviesId)
    {
        List<Map.Entry<Integer,Double>> resultList;
        //DONE:从HashMap中获取推荐内容
        resultList = GobleVar.endingResult.get(moviesId);
        for (int i=0;i<resultList.size();i++)
        {
            Map.Entry<Integer,Double> temp = resultList.get(i);
            System.out.println(temp.getKey()+":"+ GobleVar.movieList.get(temp.getKey())+":"+temp.getValue());
        }
    }
}
