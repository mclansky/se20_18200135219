package function;

import java.util.*;

public class GobleTool
{
    private static Scanner scanner = new Scanner(System.in);
    public static void printInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("==================================");
    }
    public static void printWaitInfo(Object message)
    {
        System.out.println("==================================");
        System.out.println("Info:"+message);
        System.out.println("Info:请输入任意字符按回车继续");
        System.out.println("==================================");
        scanner.next();
    }
    public static void cleanner()
    {
        //在控制台插入多行回车用于清理控制台
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n");
    }

    public static String printHelp()
    {
        String text = "";
        text += "================================================\n";
        text += "操作菜单\n";
        text += "请严格按照操作菜单的指令进行操作\n";
        text += "================================================\n";
        text += "0 - 读取推送数据结果文件至内存(读取信息)\n";
        text += "1 - 根据视频ID进行视频相似推送(视频推荐)\n";
        text += "================================================\n";
        text += "2 - 开启调试模式\n";
        text += "3 - 关闭调试模式\n";
        text += "================================================\n";
        text += "exit - 退出\n";
        text += "================================================\n";
        text += "请输入指令代码：\n";
        System.out.println(text);
        return text;
    }
}
