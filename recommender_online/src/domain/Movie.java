package domain;

import java.util.Arrays;

public class Movie
{
    private int movieId;
    private String name;
    private String[] tags;

    public Movie()
    {
    }

    public Movie(int movieId, String name, String[] tags)
    {
        this.movieId = movieId;
        this.name = name;
        this.tags = tags;
    }

    @Override
    public String toString()
    {
        return "Movie{" +
                "movieId=" + movieId +
                ", name='" + name + '\'' +
                ", tags=" + Arrays.toString(tags) +
                '}';
    }

    public int getMovieId()
    {
        return movieId;
    }

    public void setMovieId(int movieId)
    {
        this.movieId = movieId;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String[] getTags()
    {
        return tags;
    }

    public void setTags(String[] tags)
    {
        this.tags = tags;
    }
}
