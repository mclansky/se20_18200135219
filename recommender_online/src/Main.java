import function.Core;
import function.GobleVar;
import function.GobleTool;
import java.io.IOException;
import java.util.Scanner;

public class Main
{
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) throws IOException
    {
        String comment;     //用户所输入的命令
        while(true)
        {
            GobleTool.cleanner();   //清理控制台屏幕
            GobleTool.printHelp();  //输出提示信息
            comment = scanner.next();   //让用户输入指令，存到变量里面
            if (comment.equals("exit")) //判断用户的指令
                break;  //退出循环
            else if(comment.equals("0"))
                func0();
            else if(comment.equals("1"))
                func1();
            else if(comment.equals("2"))
                func2();
            else if(comment.equals("3"))
                func3();
        }
    }

    public static void func0() throws IOException
    {
        long startTime =  System.currentTimeMillis();
        //核心处理方法调用
        Core.readMovieInfo();
        long endTime =  System.currentTimeMillis();
        double usedTime = (endTime-startTime)/1000.0;
        GobleTool.printWaitInfo("本次处理用时:"+usedTime+"秒");
    }

    public static void func1()
    {
        System.out.print("请输入视频ID:");
        Integer moviesId = scanner.nextInt();
        GobleTool.printInfo("正在进行推送计算,请稍后...");
        long startTime =  System.currentTimeMillis();
        //核心处理方法调用
        Core.recommendByMoiveId(moviesId);
        long endTime =  System.currentTimeMillis();
        double usedTime = (endTime-startTime)/1000.0;
        GobleTool.printWaitInfo("本次处理用时:"+usedTime+"秒");
    }

    public static void func2() throws IOException
    {
        GobleVar.isDebug=true;
    }

    public static void func3() throws IOException
    {
        GobleVar.isDebug=false;
    }
}
